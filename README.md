# Installation

1. Run from shell: `composer install`
2. Get a Service Account key from Google Console and save it in root directory as `client_secret.json`
3. Share the Sheet with the Service Account email
4. Replace the placeholder in `index.php` with the correct Google Sheet ID