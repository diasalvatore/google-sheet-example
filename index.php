<?php
require __DIR__ . '/vendor/autoload.php';

$client = new \Google_Client();
$client->setApplicationName('Google Sheet Example');
$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
$client->setAccessType('offline');
$client->setAuthConfig(__DIR__ . '/client_secret.json');

$sheets = new \Google_Service_Sheets($client);
$spreadsheetId = ''; // placeholder
$range = "A1";
$conf = ["valueInputOption" => "RAW"];
$requestBody = new Google_Service_Sheets_ValueRange();
$requestBody->setValues(["values" => ["Hello", "World", "!!!"]]); 

$sheets->spreadsheets_values->append($spreadsheetId, $range, $requestBody, $conf);
